-module(emitm_sup).
-behaviour(supervisor).

-export([start_link/1]).
-export([init/1]).

start_link({Socks5Port, SoPort}) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, {Socks5Port, SoPort}).

init({Socks5Port, SoPort}) ->
    emitm_cert:get_root_cert(),

    {ok,
        { 
            {one_for_one, 2, 10}, 
            [
                {emitm_acceptor_sup_socks5, 
                    {emitm_acceptor_sup, start_link, [{Socks5Port, socks5}]}, 
                    permanent, 5000, worker, dynamic},

                {emitm_acceptor_sup_so, 
                    {emitm_acceptor_sup, start_link, [{SoPort, so}]}, 
                    permanent, 5000, worker, dynamic}
            ]
        }
    }.
