-module(emitm_app).
-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    ssl:start(),
    
    mnesia:stop(),
    mnesia:create_schema([node()]),
    application:ensure_all_started(mnesia),    

    emitm_cert_cache:create_table(),
    ets:new(emitm_hack_async_sni, [ordered_set, public, named_table]),
    ets:new(emitm_cache, [ordered_set, public, named_table]),
    emitm_id_serializer:start_link(),
    emitm_sup:start_link({31331, 31330}).

stop(_State) -> ok.

set_router(RouterMod) ->
    application:set_env(emitm, {router, RouterMod}).

% ====================
%  Private Functions
% ====================
