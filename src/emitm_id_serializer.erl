-module(emitm_id_serializer).
-behavior(gen_server).
%%-export([start_link/0]).
-define(CACHE_SIZE, 50000).
-compile(export_all).

code_change(_V, S, D, _E) -> {ok, S, D}.

start_link() ->
    gen_server:start_link({local, serializer}, ?MODULE, [], []).

init(Args) ->
    {ok, 1}.

handle_call(newid, From, State) ->
    %io:format("~p~n", [?CACHE_SIZE]),
    if 
        State > ?CACHE_SIZE -> 
    	    {reply, 0,  1};
	true -> 
    	    {reply, State, State + 1}
    end.

terminate(normal, State) ->
    ok.

newid() ->
    gen_server:call(serializer, newid).
