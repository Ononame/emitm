-module(emitm_socks5).
-compile(export_all).

do_client_handshake(Socket, Timeout) ->
    inet:setopts(Socket, [binary]),
    {ok, <<5,1,0>>} = gen_tcp:recv(Socket, 3, Timeout),
    ok = gen_tcp:send(Socket, <<5, 0>>),
    {ok, <<5,1,0>>} = gen_tcp:recv(Socket, 3, Timeout),
    {ok, AddrType} = gen_tcp:recv(Socket, 1, Timeout),
    Ip = case AddrType of
        <<1>> ->
            {ok, <<A:8,B:8,C:8,D:8>>} = gen_tcp:recv(Socket, 4, Timeout),
            unicode:characters_to_binary(inet:ntoa({A,B,C,D}));

        <<3>> ->
            {ok, <<DomainLen:8>>} = gen_tcp:recv(Socket, 1, Timeout),
            {ok, DomainNameBin} = gen_tcp:recv(Socket, DomainLen, Timeout),
            DomainName = unicode:characters_to_list(DomainNameBin),
            {ok, {hostent, _DomainName,_,_,_,Ips}} = inet_res:gethostbyname(DomainName),
            unicode:characters_to_binary(inet:ntoa(hd(Ips)))
    end,
    {ok, <<Port:16>>} = gen_tcp:recv(Socket, 2, Timeout),
    ok = gen_tcp:send(Socket, <<5,0,0,1,0,0,0,0,0,0>>),
    {Ip, Port}.

do_server_handshake(TargetAddr, TargetPort, Socket, Timeout) ->
    inet:setopts(Socket, [binary, {active, false}]),
    ok = gen_tcp:send(Socket, <<5,1,0>>),
    {ok, <<5,0>>} = gen_tcp:recv(Socket, 2, Timeout),
    ok = gen_tcp:send(Socket, <<5,1,0>>),
    TargetAddrType = case inet:parse_address(unicode:characters_to_list(TargetAddr)) of
        {error, einval} -> <<3>>;
        {ok, _} -> <<1>>
    end,
    ok = gen_tcp:send(Socket, TargetAddrType),
    case TargetAddrType of
        <<1>> ->
            {ok, {IPA, IPB, IPC, IPD}} = inet:parse_address(unicode:characters_to_list(TargetAddr)),
            ok = gen_tcp:send(Socket, <<IPA, IPB, IPC, IPD>>);
        <<3>> ->
            ok = gen_tcp:send(Socket, <<(byte_size(TargetAddr)):8>>),
            ok = gen_tcp:send(Socket, TargetAddr)
    end,
    ok = gen_tcp:send(Socket, <<TargetPort:16>>),
    {ok, <<5,0,0,RAddrType/binary>>} = gen_tcp:recv(Socket, 4, Timeout),
    case RAddrType of
        <<1>> ->
            {ok, _} = gen_tcp:recv(Socket, 4, Timeout);
        <<3>> ->
            {ok, RTarLen} = gen_tcp:recv(Socket, 1, Timeout),
            {ok, _} = gen_tcp:recv(Socket, RTarLen, Timeout) 
    end,
    {ok, _} = gen_tcp:recv(Socket, 2, Timeout),
    ok.
    



