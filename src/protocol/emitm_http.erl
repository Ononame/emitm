-module(emitm_http).
-compile(export_all).

-define(HTTP_MAX_HEADER_SIZE, 8000).

-define(B(List), unicode:characters_to_binary(List)).
-define(B(AA, BB), iolist_to_binary([AA, BB])).

-define(I(Term), if 
    is_binary(Term) -> binary_to_integer(Term); 
    is_list(Term) -> list_to_integer(Term);
    true -> Term
end).

headers_map_to_binary(Headers) ->
    Headers2 = maps:fold(
        fun(K,V,Acc) when is_atom(K), is_atom(V) ->
                Acc#{atom_to_binary(K, latin1)=> atom_to_binary(V, latin1)};
            (K,V,Acc) when is_atom(K) ->
                Acc#{atom_to_binary(K, latin1)=> V};
            (K,V,Acc) when is_atom(V) ->
                Acc#{K=> atom_to_binary(V, latin1)};
            (K,V,Acc) ->
                Acc#{K=> V}
        end, #{}, Headers),
    maps:fold(
        fun(K,V,Acc) ->
            if
                is_list(K), is_binary(V) ->
                    KB1 = unicode:characters_to_binary(K),
                    <<Acc/binary, KB1/binary, ": ", V/binary,"\r\n">>;
                is_binary(K), is_list(V) ->
                    VB1 = unicode:characters_to_binary(V),
                    <<Acc/binary, K/binary, ": ", VB1/binary,"\r\n">>;

                is_list(K), is_list(V) ->
                    <<Acc/binary, (unicode:characters_to_binary(K ++ ": " ++ V ++ "\r\n"))/binary>>;
                is_binary(K), is_binary(V) ->
                    <<Acc/binary, K/binary, ": ", V/binary, "\r\n">>
            end
        end, <<>>, Headers2).


build_request(Type, Path, Query, {Hv1, Hv2}, Headers, Body) ->
    Head = <<Type/binary, " ", 
        (unicode:characters_to_binary(Path))/binary, 
        (unicode:characters_to_binary(Query))/binary, 
        " HTTP/",(integer_to_binary(Hv1))/binary,".",(integer_to_binary(Hv2))/binary,"\r\n">>,

    Headers2 = maps:put(<<"Content-Length">>, integer_to_binary(byte_size(Body)), Headers),

    HeaderBin = headers_map_to_binary(Headers2),

    <<Head/binary, HeaderBin/binary, "\r\n", Body/binary>>.

build_response({V1,V2}, Code, CodeString, Headers, Body) ->
    <<"HTTP/",(integer_to_binary(V1))/binary,".",(integer_to_binary(V2))/binary," ",
        (integer_to_binary(Code))/binary," ",CodeString/binary,"\r\n",
        (headers_map_to_binary(Headers))/binary, "\r\n",
        Body/binary
    >>.


get_response(Socket, Timeout) ->
    ok = transport_setopts(Socket, [{active, false}, {packet, http_bin}]),
    case transport_recv(Socket, 0, Timeout) of
        {ok, {http_error, Body}} -> {http_error, Body};
        {ok, {http_response, _, StatusCode, _}} -> 
            HttpHeaders = recv_headers(Socket, Timeout),
            Body = recv_body(Socket, Timeout, HttpHeaders),
            {ok, StatusCode, HttpHeaders, Body};

        F -> F 
    end.

recv_headers(Socket, Timeout) ->
    ok = transport_setopts(Socket, [{active, false}, {packet, httph_bin}]),
    recv_headers_1(Socket, Timeout).

recv_headers_1(Socket, Timeout) -> recv_headers_1(Socket, Timeout, #{}, 0).
recv_headers_1(_, _, _, Size) when Size > ?HTTP_MAX_HEADER_SIZE -> throw(max_header_size_exceeded);
recv_headers_1(Socket, Timeout, Map, Size) ->
    case transport_recv(Socket, 0, Timeout) of
        {ok, http_error, Resp} -> {httph_error, Resp};
        {ok, {http_header, _, Key, undefined, Value}} -> 
            KeyBin = case is_atom(Key) of true -> atom_to_binary(Key, latin1); false -> Key end,
            recv_headers_1(Socket, Timeout, Map#{Key=>Value}, Size + byte_size(KeyBin) + byte_size(Value));
        {ok, http_eoh} -> Map
    end.


recv_body(Socket, Timeout, #{'Transfer-Encoding':= <<"chunked">>}) ->
    recv_body_chunked(Socket, Timeout);
recv_body(Socket, Timeout, #{'Content-Length':= ContLen}) ->
    recv_body_content_length(Socket, Timeout, ContLen);
recv_body(Socket, _Timeout, #{'Upgrade':= <<"websocket">>}) ->
    ok = transport_setopts(Socket, [{active, false}, {packet, raw}, binary]),
    <<>>;
recv_body(Socket, Timeout, #{'Connection':= <<"close">>}) ->
    recv_body_full(Socket, Timeout);
recv_body(_Socket, _Timeout, _) ->
    <<>>.

recv_body_chunked(Socket, Timeout) -> recv_body_chunked(Socket, Timeout, <<>>).
recv_body_chunked(Socket, Timeout, Acc) ->
    ok = transport_setopts(Socket, [{active, false}, {packet, line}, binary]),
    case transport_recv(Socket, 0, Timeout) of
        %TODO: Check for 'Trailer:' header
        {ok, <<"0\r\n">>} ->
            _ = transport_recv(Socket, 2, Timeout),
            Acc;

        {ok, ChunkSize} -> 
            ChunkSizeReal = binary:part(ChunkSize, 0, byte_size(ChunkSize)-2),
            ChunkSizeInt = httpd_util:hexlist_to_integer(binary_to_list(ChunkSizeReal)),
            ok = transport_setopts(Socket, [{active, false}, {packet, raw}, binary]),
            {ok, Chunk} = transport_recv(Socket, ChunkSizeInt, Timeout),
            _ = transport_recv(Socket, 2, Timeout),
            recv_body_chunked(Socket, Timeout, <<Acc/binary, Chunk/binary>>)
    end.

recv_body_content_length(Socket, Timeout, ContLen) ->
    ok = transport_setopts(Socket, [{active, false}, {packet, raw}, binary]),
    {ok, Body} = transport_recv(Socket, ?I(ContLen), Timeout),
    Body.

recv_body_full(Socket, Timeout) -> recv_body_full(Socket, Timeout, <<>>).
recv_body_full(Socket, Timeout, Acc) ->
    ok = transport_setopts(Socket, [{active, false}, {packet, raw}, binary]),
    case transport_recv(Socket, 0, Timeout) of
        {ok, Body} ->
            recv_body_full(Socket, Timeout, <<Acc/binary, Body/binary>>);
        {error, closed} -> Acc
    end.




transport_setopts(SSLSocket={sslsocket, _, _}, Opts) -> ssl:setopts(SSLSocket, Opts);
transport_setopts(Socket, Opts) -> inet:setopts(Socket, Opts).

transport_send(SSLSocket={sslsocket, _, _}, Payload) -> ssl:send(SSLSocket, Payload);
transport_send(Socket, Payload) -> gen_tcp:send(Socket, Payload).

transport_recv(SSLSocket={sslsocket, _, _}, M, T) -> ssl:recv(SSLSocket, M, T);
transport_recv(Socket, M, T) -> gen_tcp:recv(Socket, M, T).

transport_close(SSLSocket={sslsocket, _, _}) -> ssl:close(SSLSocket);
transport_close(Socket) -> gen_tcp:close(Socket).

transport_peername(SSLSocket={sslsocket, _, _}) -> ssl:peername(SSLSocket);
transport_peername(Socket) -> inet:peername(Socket).