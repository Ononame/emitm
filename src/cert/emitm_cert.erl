-module(emitm_cert).

-export([get_mitm_cert/1]).
-export([get_root_cert/0]).
-export([gen_root_cert/0]).

-include("cert_cache.hrl").
-include_lib("public_key/include/public_key.hrl").

gen_root_cert() ->
    io:format("~p: No root cert found~n", [?MODULE]),

    {{Y,M,D}, _} = calendar:local_time(),
    {Cert, PrivKey} = emitm_make_cert:make_cert([
        {digest, sha256},
        {validity, {{Y,M,D}, {Y+100,M,D}}},
        {version, 2},
        {subject, [
            {name, "emitm Root Certificate"},
            {email, "emitm@emitm.com"},
            {city, "Minnetonka"},
            {state, "Minnesota"},
            {org, "emitm"},
            {org_unit, "emitm"},
            {country, "US"},
            {title, "emitm"},
            {dnQualifer, "emitm.com"}
        ]},
        {issuer, true},
        {key, rsa},
        {extensions, [{basic_constraints, default}]}
    ]),
    OTPCert = public_key:pkix_decode_cert(Cert, otp),

    emitm_make_cert:write_pem("./priv", "emitm", {Cert, PrivKey}),
    ok = mnesia:dirty_write(#cert_cache{thumbprint=root, 
        fake_otp_cert=OTPCert, fake_cert=Cert, fake_key=PrivKey}),

    io:format("~p: Generated root cert in ./priv/[emitm.pem, emitm_key.pem]~n", [?MODULE]),

    {Cert, PrivKey}.

get_root_cert() ->
    case mnesia:dirty_read(cert_cache, root) of
        [RootCert] -> {RootCert#cert_cache.fake_cert, RootCert#cert_cache.fake_key};
        [] -> gen_root_cert()
    end.


get_mitm_cert(Socket) ->
    {ok, DestCert} = ssl:peercert(Socket),
    <<ThumbPrint:128/big-unsigned-integer>> = crypto:hash(md5, DestCert),
    get_mitm_cert_1(ThumbPrint, DestCert, mnesia:dirty_read(cert_cache, ThumbPrint)).

get_mitm_cert_1(_, _, [Cert]) -> {Cert#cert_cache.fake_cert, Cert#cert_cache.fake_key};
get_mitm_cert_1(ThumbPrint, DestCert, []) -> 
    {RootCert, RootPrivKey} = get_root_cert(),
    RootPrivKeyDecoded = emitm_make_cert:decode_key(RootPrivKey),

    RootCertDecoded = public_key:pkix_decode_cert(RootCert, otp),
    #'OTPCertificate'{tbsCertificate=RootTbs} = RootCertDecoded,
    #'OTPTBSCertificate'{subject=RootSubject} = RootTbs,

    {NewKey, NewKeyEncoded} = emitm_make_cert:gen_rsa(64),
    OTPCert = public_key:pkix_decode_cert(DestCert, otp),
    TBSCert = OTPCert#'OTPCertificate'.tbsCertificate,
    TBSCert2 = TBSCert#'OTPTBSCertificate'{
        issuer=RootSubject,
        subjectPublicKeyInfo= emitm_make_cert:publickey(NewKey)
    },

    Cert = public_key:pkix_sign(TBSCert2, RootPrivKeyDecoded),

    ok = mnesia:dirty_write(#cert_cache{
        thumbprint= ThumbPrint, fake_key= NewKeyEncoded, fake_cert= Cert
    }),
    {Cert, NewKeyEncoded}.