-module(emitm_cert_cache).
-compile(export_all).

-include("cert_cache.hrl").

create_table() ->
    Res = mnesia:create_table(cert_cache, [
        {type, ordered_set},
        {disc_copies, [node()]},
        {attributes, record_info(fields, cert_cache)}]
    ),
    io:format("create_table cert_cache result: ~p~n", [Res]),
    mnesia:wait_for_tables(mnesia:system_info(local_tables), infinity).
