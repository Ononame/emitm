-module(emitm_basic_router).
-compile(export_all).

route(Hostname, Addr, Port, State) ->
    io:format("~p: Basic route hit ~p ~p ~p returning forward ~n", 
        [?MODULE, Hostname, Addr, Port]),
    io:format("~p: State ~p~n", [?MODULE, State]),
    %{forward, State}.
    case Port of
        80 -> 
            {{mitm_http, emitm_basic_router}, State};
            %{forward, State};
        443 -> 
            %{forward, State};
            {{mitm_https, emitm_basic_router}, State};
        _ -> {forward, State}
    end.

onconnect(State) -> State.
ondisconnect(State) -> State.


onclient_server(State) ->
    %io:format("~p: ~p ~p~n", [?MODULE, Type, Arg]),
    State.

onserver_client(State) ->
    %io:format("~p: ~p ~p~n", [?MODULE, Type, Arg]),
    State.
