-module(emitm_connection_gsm).
-behaviour(gen_statem).
-compile(export_all).

callback_mode() -> handle_event_function.

start(P) -> gen_statem:start(?MODULE, P, []).
start_link(P) -> gen_statem:start_link(?MODULE, P, []).

code_change(_V, S, D, _E) -> {ok, S, D}.
format_status(_Opt, [PDict, State, _Data]) -> {State, proplists:get_value('$ancestors', PDict)}.
terminate(_R, _S, _D) -> 
    %io:format("~p: Terminate ~p~n", [?MODULE, _S]),
    case maps:get(module, _D, undefined) of
        undefined -> ok;
        Mod -> erlang:apply(Mod, ondisconnect, [_D])
    end.

init([Type, Id]) ->
    %process_flag(trap_exit, true),
    %io:format("~p: init ~p~n", [?MODULE, Type]),
    {ok, 
        step0_wait_socket,
        #{type=> Type, id => Id}
    }.

%handle closures
handle_event(info, {tcp_closed, _}, _, D) ->
    {stop, {shutdown, tcp_closed}, D};
handle_event(info, {ssl_closed, _}, _, D) ->
    {stop, {shutdown, ssl_closed}, D};
handle_event(info, {tcp_error, _, _Err}, _, D) ->
    {stop, {shutdown, tcp_error}, D};
handle_event(info, {ssl_error, _, _Err}, _, 
    D=#{dest_host:= DstHost, dest:={_,DestPort,_}}) 
->
    io:format("~p: ssl_error ~p ~p ~p~n", [?MODULE, _Err, DstHost, DestPort]),
    {stop, {shutdown, ssl_error}, D};


%ignore everything in hang mode
handle_event(_, _, hang, D) -> {next_state, hang, D};

%forward everything in forward mode
handle_event(info, {tcp_closed, _}, forward, D) -> {stop, tcp_closed, D};
handle_event(info, {tcp, CS, Bin}, forward, D=#{source:= {_,_,CS}, dest:= {_,_,SS}}) 
-> 
    ok = gen_tcp:send(SS, Bin),
    {next_state, forward, D};
handle_event(info, {tcp, SS, Bin}, forward, D=#{source:= {_,_,CS}, dest:= {_,_,SS}}) 
-> 
    ok = gen_tcp:send(CS, Bin),
    {next_state, forward, D};

%mitm_raw
%handle this properly all exit signals
handle_event(info, {tcp_closed, _}, mitm_raw, D=#{module:= Mod}) -> 
    %{Bin2, D2} = apply(Mod, closed, [Dir, State2]),
    {stop, tcp_closed, D};

handle_event(info, {tcp, CS, Bin}, mitm_raw, 
    D=#{source:= {_,_,CS}, dest:= {_,_,SS}, module:= Mod}) 
-> 
    {Bin2, D2} = apply(Mod, request, [D#{request => #{raw => Bin}}]),
    ok = gen_tcp:send(SS, Bin2),
    {next_state, mitm_raw, D2};

handle_event(info, {tcp, SS, Bin}, mitm_raw, 
    D=#{source:= {_,_,CS}, dest:= {_,_,SS}, module:= Mod}) 
-> 
    {Bin2, D2} = apply(Mod, response, [D#{response => #{raw => Bin}}]),
    ok = gen_tcp:send(CS, Bin2),
    {next_state, mitm_raw, D2};


%mitm_http
%chuhnked
handle_event(info, {tcp, CS, Bin}, mitm_http_chunk_header, 
    D=#{source:= {_,_,CS}, dest:= {_,_,SS}, module:= Mod})
->
    ChunkSizeReal = binary:part(Bin, 0, byte_size(Bin)-2),
    ChunkSizeInt = httpd_util:hexlist_to_integer(binary_to_list(ChunkSizeReal)),
    case ChunkSizeInt of
        0 ->
            _ = gen_tcp:recv(CS, 2, 30000),
            ok = inet:setopts(CS, [{active, once}, {packet, http_bin}]),

            ok = gen_tcp:send(SS, <<Bin/binary, "\r\n">>),
            {next_state, mitm_http, D};

        _ ->
            ok = inet:setopts(CS, [{active, false}, {packet, raw}, binary]),
            {ok, Chunk} = gen_tcp:recv(CS, ChunkSizeInt+2, 30000),
            %_ = transport_recv(Socket, 2, 30000),

            %{Chunk2, D2} = apply(Mod, request, [https_chunk, Chunk, D]),
            D2 = #{request:= #{chunk:=Chunk2}} 
	       = apply(Mod, request, [D#{type => http_chunk, request => #{chunk => Chunk}}]),

            ok = inet:setopts(CS, [{active, once}, {packet, line}, binary]),

            ok = gen_tcp:send(SS, Bin),
            ok = gen_tcp:send(SS, Chunk2),

            {next_state, mitm_http_chunk_header, D2}
    end;

handle_event(info, {tcp, SS, Bin}, mitm_http_chunk_header, 
    D=#{source:= {_,_,CS}, dest:= {_,_,SS}, module:= Mod})
->
    ChunkSizeReal = binary:part(Bin, 0, byte_size(Bin)-2),
    ChunkSizeInt = httpd_util:hexlist_to_integer(binary_to_list(ChunkSizeReal)),
    case ChunkSizeInt of
        0 ->
            _ = gen_tcp:recv(SS, 2, 30000),
            ok = inet:setopts(SS, [{active, once}, {packet, http_bin}]),

            ok = gen_tcp:send(CS, <<Bin/binary, "\r\n">>),
            {next_state, mitm_http, D};

        _ ->
            ok = inet:setopts(SS, [{active, false}, {packet, raw}, binary]),
            {ok, Chunk} = gen_tcp:recv(SS, ChunkSizeInt+2, 30000),
            %_ = transport_recv(Socket, 2, 30000),

            %{Chunk2, D2} = apply(Mod, request, [https_chunk, Chunk, D]),
            D2 = #{response:= #{chunk:=Chunk2}} 
	       = apply(Mod, response, [D#{type => http_chunk, response => #{chunk => Chunk}}]),

            ok = inet:setopts(SS, [{active, once}, {packet, line}, binary]),

            ok = gen_tcp:send(CS, Bin),
            ok = gen_tcp:send(CS, Chunk2),

            {next_state, mitm_http_chunk_header, D2}
    end;
%end chunked
handle_event(info, {http, CS, {http_request, Type, {abs_path, Path}, HttpVer}}, mitm_http,
    D=#{source:= {_,_,CS}, module:= Mod})
->
    TypeBin = atom_to_binary(Type, latin1),

    HttpHeaders = emitm_http:recv_headers(CS, 30000),
    {Body, Chunk} = case maps:get('Transfer-Encoding', HttpHeaders, undefined) of
        undefined ->
            {emitm_http:recv_body(CS, 30000, HttpHeaders), false};
        <<"chunked">> ->
            {<<>>, true}
    end,

    D2 = #{dest:= {DestAddrBin, DestPort}, 
	   type:= TypeBin2, 
	   host:= Host2,
	   path:=Path2, 
	   httpver:= HttpVer2, 
	   request:= 
	     #{headers:= HttpHeaders2, 
	       body:= Body2}} 
       = apply(Mod, request, [D#{host=>maps:get('Host', HttpHeaders, undefined), 
				 type=>TypeBin, 
				 path=>Path, 
				 httpver => HttpVer,
				 request => 
				 #{ 
				   headers => HttpHeaders, 
				   body => Body}}]),

    RebuiltRequest = emitm_http:build_request(TypeBin2, Path2, <<>>, HttpVer, HttpHeaders2, Body2),
    DestAddr = erlang:binary_to_list(DestAddrBin),
    case DestPort of
    	443 ->
	    %use https here
	    %io:format("~p~n", [erlang:is_binary(maps:get('Host', HttpHeaders2))]),
	    %io:fwrite(RebuiltRequest),
            {ok, SSLServerSocket} = ssl:connect(
                DestAddr, 443,
                [
		    {reuse_sessions, false},
                    {active, once}, 
                    {nodelay, true},
		    {verify, verify_none},
                    {versions, ['tlsv1.2', 'tlsv1.1', tlsv1, sslv3]},
		    %{ciphers, ssl:cipher_suites(all)},
                    {server_name_indication, erlang:binary_to_list(Host2)}, 
                    binary
                ], 30000),
	    ssl:setopts(SSLServerSocket, [{packet, http_bin}]),
	    ssl:send(SSLServerSocket, RebuiltRequest),
	    {next_state, mitm_https, D2#{dest:= {DestAddrBin, DestPort, SSLServerSocket}}};
         _ ->
	    {ok, SS}=gen_tcp:connect(DestAddr, DestPort, [{active, once}, binary, {nodelay, true}]),
	    gen_tcp:send(SS, RebuiltRequest),
    	    case Chunk of
    	        false ->
    	            ok = inet:setopts(SS, [{active, once}, {packet, http_bin}]),
    	            {next_state, mitm_http, D2#{dest:= {DestAddrBin, DestPort, SS}}};
    	        true ->
    	            ok = inet:setopts(SS, [{active, once}, {packet, line}, binary]),
    	            {next_state, mitm_http_chunk_header, D2#{dest:= {DestAddrBin, DestPort, SS}}}
    	    end
	    %use http 
    end;

    %ok = gen_tcp:send(SS, RebuiltRequest),


handle_event(info, {http, SS, {http_response, HttpVer, Code, HttpString}}, mitm_http,
    D=#{source:= {_,_,CS}, dest:= {_,_,SS}, module:= Mod})
->
    HttpHeaders = emitm_http:recv_headers(SS, 30000),
    {Body, Chunk} = case maps:get('Transfer-Encoding', HttpHeaders, undefined) of
        undefined ->
            {emitm_http:recv_body(SS, 30000, HttpHeaders), false};
        <<"chunked">> ->
            {<<>>, true}
    end,

    %{{{Code2, HttpString2, HttpVer2}, HttpHeaders2, Body2}, D2} 
    %    = apply(Mod, response, [http, {{Code, HttpString, HttpVer}, HttpHeaders, Body}, D]),

    D2 = #{response:= 
	     #{httpver:= HttpVer2,
	       httpstring:=HttpString2,
	       code:=Code2,
	       headers:= HttpHeaders2, 
	       body:= Body2}} 
       = apply(Mod, response, [D#{response => 
				  #{code=>Code, 
				    httpstring=> HttpString, 
				    httpver => HttpVer, 
				    headers => HttpHeaders, 
				    body => Body}}]),

    RebuiltResponse = emitm_http:build_response(HttpVer2, Code2, HttpString2, HttpHeaders2, Body2),
    ok = gen_tcp:send(CS, RebuiltResponse),

    case Chunk of
        false ->
            ok = inet:setopts(SS, [{active, once}, {packet, http_bin}]),
            {next_state, mitm_http, D2};
        true ->
            ok = inet:setopts(SS, [{active, once}, {packet, line}, binary]),
            {next_state, mitm_http_chunk_header, D2}
    end;


%mitm_https
%chuhnked
handle_event(info, {ssl, CS, Bin}, mitm_https_chunk_header, 
    D=#{source:= {_,_,CS}, dest:= {_,_,SS}, module:= Mod})
->
    ChunkSizeReal = binary:part(Bin, 0, byte_size(Bin)-2),
    ChunkSizeInt = httpd_util:hexlist_to_integer(binary_to_list(ChunkSizeReal)),
    case ChunkSizeInt of
        0 ->
            _ = ssl:recv(CS, 2, 30000),
            ok = ssl:setopts(CS, [{active, once}, {packet, http_bin}]),

            ok = ssl:send(SS, <<Bin/binary, "\r\n">>),
            {next_state, mitm_https, D};

        _ ->
            ok = ssl:setopts(CS, [{active, false}, {packet, raw}, binary]),
            {ok, Chunk} = ssl:recv(CS, ChunkSizeInt+2, 30000),
            %_ = transport_recv(Socket, 2, 30000),

            %{Chunk2, D2} = apply(Mod, request, [https_chunk, Chunk, D]),
            D2 = #{request:= #{chunk:=Chunk2}} 
	       = apply(Mod, request, [D#{type => https_chunk, request => #{chunk => Chunk}}]),

            ok = ssl:setopts(CS, [{active, once}, {packet, line}, binary]),

            ok = ssl:send(SS, Bin),
            ok = ssl:send(SS, Chunk2),

            {next_state, mitm_https_chunk_header, D2}
    end;

handle_event(info, {ssl, SS, Bin}, mitm_https_chunk_header, 
    D=#{source:= {_, Port, CS}, dest:= {_,_,SS}, module:= Mod})
->
    ChunkSizeReal = binary:part(Bin, 0, byte_size(Bin)-2),
    ChunkSizeInt = httpd_util:hexlist_to_integer(binary_to_list(ChunkSizeReal)),
    case ChunkSizeInt of
        0 ->
            _ = ssl:recv(SS, 2, 30000),
            ok = ssl:setopts(SS, [{active, once}, {packet, http_bin}]),

            ok = ssl:send(CS, <<Bin/binary, "\r\n">>),
            {next_state, mitm_https, D};

        _ ->
            ok = ssl:setopts(SS, [{active, false}, {packet, raw}, binary]),
            {ok, Chunk} = ssl:recv(SS, ChunkSizeInt+2, 30000),
            %_ = transport_recv(Socket, 2, 30000),

            %{Chunk2, D2} = apply(Mod, request, [https_chunk, Chunk, D]),
            D2 = #{response:= #{chunk:=Chunk2}} 
	       = apply(Mod, response, [D#{type => https_chunk, response => #{chunk => Chunk}}]),

            ok = ssl:setopts(SS, [{active, once}, {packet, line}, binary]),

	    case CS of
	      {sslsocket, _, _} ->
            	ok = ssl:send(CS, Bin),
            	ok = ssl:send(CS, Chunk);
	      _ ->
	        ok = gen_tcp:send(CS, Bin),
		ok = gen_tcp:send(CS, Chunk)
	    end,

            {next_state, mitm_https_chunk_header, D2}
    end;
%end chunked
handle_event(info, {ssl, CS, {http_request, Type, {abs_path, Path}, HttpVer}}, mitm_https,
    D=#{source:= {_,_,CS}, dest:= {_,_,SS}, module:= Mod})
->
    TypeBin = atom_to_binary(Type, latin1),

    HttpHeaders = emitm_http:recv_headers(CS, 30000),
    {Body, Chunk} = case maps:get('Transfer-Encoding', HttpHeaders, undefined) of
        undefined ->
            {emitm_http:recv_body(CS, 30000, HttpHeaders), false};
        <<"chunked">> ->
            {<<>>, true}
    end,

    %{{{TypeBin2, Path2, HttpVer2}, HttpHeaders2, Body2}, D2} 
    %    = apply(Mod, request, [https, {{TypeBin, Path, HttpVer}, HttpHeaders, Body}, D]),
        D2 = #{dest:= {DestAddrBin, DestPort, _}, 
    	   type:= TypeBin2, 
    	   host:= Host2,
    	   path:=Path2, 
    	   httpver:= HttpVer2, 
    	   request:= 
    	     #{headers:= HttpHeaders2, 
    	       body:= Body2}} 
           = apply(Mod, request, [D#{host=>maps:get('Host', HttpHeaders, undefined), 
    				 type=>TypeBin, 
    				 path=>Path, 
    				 httpver => HttpVer,
    				 request => 
    				 #{ 
    				   headers => HttpHeaders, 
    				   body => Body}}]),

    RebuiltRequest = emitm_http:build_request(TypeBin2, Path2, <<>>, HttpVer, HttpHeaders2, Body2),
    ok = ssl:send(SS, RebuiltRequest),

    case Chunk of
        false ->
            ok = ssl:setopts(CS, [{active, once}, {packet, http_bin}]),
            {next_state, mitm_https, D2};
        true ->
            ok = ssl:setopts(CS, [{active, once}, {packet, line}, binary]),
            {next_state, mitm_https_chunk_header, D2}
    end;

handle_event(info, {ssl, SS, {http_response, HttpVer, Code, HttpString}}, mitm_https,
    D=#{source:= {_,_,CS}, dest:= {_,_,SS}, module:= Mod})
->
    HttpHeaders = emitm_http:recv_headers(SS, 30000),
    {Body, Chunk} = case maps:get('Transfer-Encoding', HttpHeaders, undefined) of
        undefined ->
            {emitm_http:recv_body(SS, 30000, HttpHeaders), false};
        <<"chunked">> ->
            {<<>>, true}
    end,
    %io:format("~p~p~n", [HttpHeaders, Body]),
    %{{{Code2, HttpString2, HttpVer2}, HttpHeaders2, Body2}, D2} 
    %    = apply(Mod, response, [https, {{Code, HttpString, HttpVer}, HttpHeaders, Body}, D]),
    
    D2 = #{response:= 
	     #{httpver:= HttpVer2,
	       httpstring:=HttpString2,
	       code:=Code2,
	       headers:= HttpHeaders2, 
	       body:= Body2}} 
       = apply(Mod, response, [D#{response => 
				  #{code=>Code, 
				    httpstring=> HttpString, 
				    httpver => HttpVer, 
				    headers => HttpHeaders, 
				    body => Body}}]),

    RebuiltResponse = emitm_http:build_response(HttpVer2, Code2, HttpString2, HttpHeaders2, Body2),
    case CS of 
      {sslsocket, _, _} ->
	    ssl:send(CS, RebuiltResponse);
    	    %{next_state, mitm_https, D1};
	_->
	    gen_tcp:send(CS, RebuiltResponse)
	    %{next_state, mitm_http, D1}
    end,
    %ok = ssl:send(CS, RebuiltResponse),

    case Chunk of
        false ->
            ok = ssl:setopts(SS, [{active, once}, {packet, http_bin}]),
            {next_state, mitm_https, D2};
        true ->
            ok = ssl:setopts(SS, [{active, once}, {packet, line}, binary]),
            {next_state, mitm_https_chunk_header, D2}
    end;


handle_event(info, {ssl, SS, Bin}, mitm_https,
    D=#{source:= {_, Port, CS}, module:= Mod})
-> 
    D1 = apply(Mod, response, [D#{response => Bin}]),
    case CS of 
	{sslsocket, _, _}->
	    ssl:send(CS, Bin),
    	    {next_state, mitm_https, D1};
	_->
	    gen_tcp:send(CS, Bin),
	    {next_state, mitm_http, D1}
    end;


handle_event(info, {pass_socket, ClientSocket}, step0_wait_socket, D=#{type:= socks5}) ->
    {ok, {SourceAddr, SourcePort}} = inet:peername(ClientSocket),
    SourceAddrBin = unicode:characters_to_binary(inet_parse:ntoa(SourceAddr)),

    {DestAddrBin, DestPort} = emitm_socks5:do_client_handshake(ClientSocket, 30000),

    route(
        {SourceAddrBin, SourcePort, ClientSocket}, 
        {DestAddrBin, DestPort, undefined}, 
        D);

handle_event(info, {pass_socket, ClientSocket}, step0_wait_socket, D=#{type:= so}) ->
    %io:format("~p: got socket ~p ~n", [?MODULE, ClientSocket]),
    {ok, {SourceAddr, SourcePort}} = inet:peername(ClientSocket),
    SourceAddrBin = unicode:characters_to_binary(inet_parse:ntoa(SourceAddr)),

    %SO_ORIGINAL_DST
    {ok, [{raw,0,80,Info}]} = inet:getopts(ClientSocket,[{raw, 0, 80, 16}]),
    <<_:16, DestPort:16/big, A:8, B:8, C:8, DD:8, _/binary>> = Info,
    DestAddr = {A,B,C,DD},
    DestAddrBin = unicode:characters_to_binary(inet_parse:ntoa(DestAddr)),

    route(
        {SourceAddrBin, SourcePort, ClientSocket}, 
        {DestAddrBin, DestPort, undefined}, 
        D).

route(
    {SourceAddrBin, SourcePort, ClientSocket}, 
    {DestAddrBin, DestPort, undefined}, 
    D) 
->
    DestAddr = unicode:characters_to_list(DestAddrBin),
    %DestHostname = case inet_res:gethostbyaddr(DestAddr) of
    %    {ok, {hostent, Dest, _, _, _, _}} -> Dest;
    %    _ -> <<>>
    %end,
    %DestHostname = <<>>,
    {Route, D2} = case application:get_env(emitm, router) of
        undefined -> throw(io_lib:format("~p:~p No router defined~n", [?MODULE, ?LINE]));
        {ok, RouterMod} ->
            apply(RouterMod, route, [
                DestAddrBin, DestPort,
                D#{
                    source=> {SourceAddrBin, SourcePort, ClientSocket}, 
                    dest=> {DestAddrBin, DestPort, undefined}
                }
            ])
    end,

    %io:format("~p: Connection to ~p ~p ~p resolved as ~p ~n", 
    %    [?MODULE, DestHostname, DestAddrBin, DestPort, Route]),

    case Route of
        drop -> {stop, connection_dropped, D2};
        hang -> 
            ok = inet:setopts(ClientSocket, [{active,true}]),
            {next_state, hang, D2};

        forward -> 
            ok = inet:setopts(ClientSocket, [{active,true}, binary]),
            {ok, DestSocket} = gen_tcp:connect(DestAddr, DestPort, [{active,true}, binary, {nodelay, true}]),
            {next_state, forward, D2#{dest=> {DestAddrBin, DestPort, DestSocket}}};

        {forward, Socks5Socket} ->
            ok = inet:setopts(ClientSocket, [{active,true}, binary, {nodelay, true}]),
            ok = inet:setopts(Socks5Socket, [{active,true}, binary, {nodelay, true}]),
            {next_state, forward, D2#{dest=> {DestAddrBin, DestPort, Socks5Socket}}};

        {mitm_raw, Module} ->
            ok = inet:setopts(ClientSocket, [{active,true}, binary]),
	    %{ok, DestSocket} = gen_tcp:connect(DestAddr, DestPort, [{active,true}, binary, {nodelay, true}]),
            DestSocket = {},
	    D3 = D2#{dest=> {DestAddrBin, DestPort, DestSocket}, module=> Module},
            D4 = apply(Module, onconnect, [D3]),
            {next_state, mitm_raw, D4};

        {mitm_http, Module} ->
            ok = inet:setopts(ClientSocket, [{active, once}, {packet, http_bin}]),
	    %{ok, DestSocket} = gen_tcp:connect(DestAddr, DestPort, [{active,once}, {packet, http_bin}, {nodelay, true}]),
            D3 = D2#{dest=> {DestAddrBin, DestPort}, module=> Module},
            %D4 = apply(Module, onconnect, [D3]),
            {next_state, mitm_http, D3};

        {mitm_https, Module} ->
            ParentPID = self(),
            RandomHack = crypto:strong_rand_bytes(16),
            {ok, SSLClientSocket} = ssl:ssl_accept(ClientSocket, 
                [
                    %{log_alert, false},
                    {versions, ['tlsv1.2', 'tlsv1.1', tlsv1, sslv3]},
                    {ciphers, ssl:cipher_suites(all)},
                    %{certfile, "./priv/emitm.pem"},
                    %{keyfile, "./priv/emitm_key.pem"},
                    {sni_fun, fun(SNI) ->
                        {ok, SSLServerSocket} = ssl:connect(
                            DestAddr, DestPort,
                            [
                                {active, false}, 
                                {nodelay, true},
                                {versions, ['tlsv1.2', 'tlsv1.1', tlsv1, sslv3]},
                                {ciphers, ssl:cipher_suites(all)},
                                {server_name_indication, SNI}, 
                                binary
                            ], 30000),

                        {Cert, ClientKey} = emitm_cert:get_mitm_cert(SSLServerSocket),
                        {_, Key, not_encrypted} = ClientKey,

                        ssl:controlling_process(SSLServerSocket, ParentPID),
                        %cant use procdict here
                        ets:insert(emitm_hack_async_sni, {{ssl_dest_sock, RandomHack}, SSLServerSocket}),
                        [{key, {'RSAPrivateKey', Key}}, {cert, Cert}]
                        %[{certfile, "./priv/emitm.pem"}, {keyfile, "./priv/emitm_key.pem"}]
                    end}
                ],
                10000
            ),

            [{{ssl_dest_sock,RandomHack}, SSLServerSocket}] 
                = ets:take(emitm_hack_async_sni, {ssl_dest_sock, RandomHack}),

            D3 = D2#{
                module=> Module,
                source=> {SourceAddrBin, SourcePort, SSLClientSocket},
                dest=> {DestAddrBin, DestPort, SSLServerSocket}
            },
            D4 = apply(Module, onconnect, [D3]),

            ok = ssl:setopts(SSLClientSocket, [{active, once}, {packet, http_bin}]),
            ok = ssl:setopts(SSLServerSocket, [{active, once}, {packet, http_bin}]),
            {next_state, mitm_https, D4};

        {mitm_ws, Module} -> {stop, mitm_ws_not_implemented, D2};
        {mitm_wss, Module} -> {stop, mitm_wss_not_implemented, D2}
    end.
