-module(emitm_acceptor_gen).
-behaviour(gen_server).
-compile(export_all).

handle_cast(_Message, S) -> {noreply, S}.
handle_call(_Message, _From, S) -> {reply, ok, S}.
code_change(_OldVersion, S, _Extra) -> {ok, S}. 

terminate(_Reason, S) ->
    io:format("~p: terminated ~p~n", [?MODULE, _Reason]).

start_link(P) -> gen_server:start(?MODULE, P, []).

init({ListenSocket, Type, Id}) ->
    process_flag(trap_exit, true),
    {ok, _} = prim_inet:async_accept(ListenSocket, -1),
    {ok, #{listen_socket=> ListenSocket, type=> Type, id=> Id}}.


handle_info({inet_async, ListenSocket, _, {ok, ClientSocket}}, S=#{type:= Type}) ->
    prim_inet:async_accept(ListenSocket, -1),
    
    Id =  emitm_id_serializer:newid(),

    ets:insert(emitm_cache, {Id, #{type => Type, timestamp => os:timestamp()}}),

    {ok, Pid} = emitm_connection_gsm:start([Type, Id]),

    inet_db:register_socket(ClientSocket, inet_tcp),
    ok = gen_tcp:controlling_process(ClientSocket, Pid),

    Pid ! {pass_socket, ClientSocket},

    {noreply, S};

%TODO: Dont shutdown on all errors, see what is critical or not
handle_info({inet_async, _ListenSocket, _, Error}, S) ->
    io:format("~p: Error in inet_async accept, shutting down. ~p~n", [?MODULE, Error]),
    {stop, Error, S};

handle_info(_Message, S) -> {noreply, S}.
