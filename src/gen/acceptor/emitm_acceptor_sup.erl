-module(emitm_acceptor_sup).
-behaviour(supervisor).

-export([start_link/1, init/1]).

start_link(P) ->
    supervisor:start_link(?MODULE, P).

init({ListenPort, Type}) ->
    io:format("~p: Starting accepter on port ~p ~p~n", [?MODULE, ListenPort, Type]),
    %AcceptorCount = erlang:system_info(schedulers),
    {ok, ListenSocket} = gen_tcp:listen(ListenPort, [
        {ip, {0,0,0,0}}, {active, false}, {reuseaddr, true}, {nodelay, true}
    ]),

    AcceptorCount = 2,
    Acceptors = [
        {
            {acceptor, self(), N}, 
            {emitm_acceptor_gen, start_link, [{ListenSocket, Type, N}]}, 
            permanent, 5000, worker, []
        } || N <- lists:seq(1, AcceptorCount)
    ],

    {ok,
        { 
            {one_for_one, 2, 10},
            Acceptors
        }
    }.